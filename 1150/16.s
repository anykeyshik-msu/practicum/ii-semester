%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_UDEC 4, eax ;a11
    GET_UDEC 4, ebx ;a12
    GET_UDEC 4, ecx ;a21
    GET_UDEC 4, edx ;a22
    GET_UDEC 4, esi ;b1
    GET_UDEC 4, edi ;b2

    ; r8d = b1 | b2
    mov r8d, esi
    or r8d, edi

    ; r9d = (a11 & a22) ^ (a12 & a21)
    mov r9d, eax
    and r9d, edx
    mov r10d, ebx
    and r10d, ecx
    or r9d, r10d

    ; r8d & !r9d
    not r9d
    and r8d, r9d

    ; r9d = a11 | a21
    mov r9d, eax
    or r9d, ecx

    ; r10d = a12 | a22
    mov r10d, ebx
    or r10d, edx

    ; y = ~r8d & ( (a11 & b2) ^ (a21 & b1) ) | r8d & r10d & ~r9d
    mov r11d, eax
    and r11d, edi
    mov r12d, ecx
    and r12d, esi
    xor r11d, r12d

    not r8d
    and r11d, r8d
    not r8d
    or r11d, r8d
    and r11d, r10d
    not r9d
    and r11d, r9d
    not r9d

    PRINT_UDEC 4, r11d

    xor eax, eax
    ret
