%include "io.inc"

section .data
value db '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'
suit db 'S', 'C', 'D', 'H' 

section .text
global CMAIN
CMAIN:
    GET_UDEC 1, ax

    dec ax
    mov bl, 13
    div bl

    xor ebx, ebx
    mov bl, ah
    PRINT_CHAR [value + ebx]

    xor ebx, ebx
    mov bl, al
    PRINT_CHAR [suit + ebx]

    xor eax, eax
    ret
