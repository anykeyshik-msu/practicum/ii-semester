%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_CHAR cl
    GET_UDEC 1, ch
    GET_CHAR bl ; Space
    GET_CHAR bl
    GET_UDEC 1, bh
    
    xor ax, ax
    xor dx, dx
    mov dl, 1
    mov dh, 2
    sub cl, bl
    adc al, 0
    mul dh
    sub dl, al
    mov al, cl
    mul dl
    mov cl, al

    xor ax, ax
    xor dx, dx
    mov dl, 1
    mov dh, 2
    sub ch, bh
    adc al, 0
    mul dh
    sub dl, al
    mov al, ch
    mul dl
    mov ch, al

    add cl, ch
    PRINT_DEC 1, cl

    xor eax, eax
    ret
