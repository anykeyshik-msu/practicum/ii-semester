%include "io.inc"

section .text
global CMAIN
CMAIN:
    xor eax, eax
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx
    xor esi, esi
    xor edi, edi

    GET_UDEC 2, ax
    GET_UDEC 2, bx
    mul ebx

    GET_UDEC 2, bx
    mul ebx

    GET_UDEC 2, bx
    div ebx

    ; eax - number of boxes, edx - remainder; ebx - garbage; ecx,esi,edi - 0
    ; Compare remainder to zero
    sub ecx, edx
    adc esi, eax

    ; Clear
    xor eax, eax
    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx

    GET_UDEC 2, ax
    GET_UDEC 2, bx
    
    ; eax - number of hours, esi - number of boxes; ebx - garbage; ecx,edx,edi - 0
    ; Compare hours to 6
    mov ebx, 5
    sub bx, ax
    adc edi, 0

    ;Clear
    xor eax, eax
    xor ebx, ebx

    ; esi - number of boxes, esi - mul of houers; eax,ebx,ecx,edx - 0
    ; Calc number of denied boxes
    mov eax, esi
    mov ebx, 3
    div ebx
    
    xor ebx, ebx
    sub ebx, edx
    adc ecx, 0

    add eax, ecx

    mul edi
    sub esi, eax

    PRINT_UDEC 4, esi

    xor eax, eax
    ret
