%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_UDEC 4, ebx
    GET_UDEC 4, ecx
    GET_UDEC 4, edx
    GET_UDEC 4, eax

    sub eax, 2011
    sub edx, ecx

    mul edx
    sub ebx, eax

    PRINT_UDEC 4, ebx

    xor eax, eax
    ret
