%include "io.inc"

section .text
global CMAIN
CMAIN:
    xor ax, ax
    xor bx, bx
    xor cx, cx
    
    GET_CHAR bl
    GET_UDEC 1, bh
    
    mov al, 'A'
    dec al
    sub bl, al

    mov ax, 2056
    sub al, bl
    sub ah, bh
    mul ah
    mov cl, 2
    div cl
    
    PRINT_UDEC 1, al

    xor eax, eax
    ret
