global func

section .text

func:
    push ebp
    push ebx
    mov ebp, esp

    mov eax, [esp + 12]
    xor eax, [esp + 16]
    mov ebx, [esp + 20]
    or  ebx, [esp + 24]
    and eax, ebx

    pop ebx
    pop ebp
    ret
