%include "io.inc"

section .bss
    matrix resd 64

section .text
global CMAIN
CMAIN:
    xor eax, eax

    .iLoop:
        xor ebx, ebx
    .jLoop:
        GET_UDEC 4, ecx
        mov [matrix + eax + ebx], ecx

        add ebx, 4
        cmp ebx, 32
        jl .jLoop

        add eax, 32
        cmp eax, 256
        jl .iLoop

    mov eax, 7
    push eax
    push eax
    xor edx, edx
    push edx
    mov eax, -1
    call find_path
    add esp, 12

    PRINT_UDEC 4, eax

    xor eax, eax
    ret

find_path:
    push ebp
    push ebx
    mov ebp, esp

    mov ebx, [esp + 20]
    mov ecx, [esp + 16]
    mov edx, [esp + 12]
    ; ebx - i; ecx - j
    lea esi, [ebx * 4]
    lea esi, [esi * 4]
    lea esi, [esi * 2]
    lea edi, [ecx * 4]
    PRINT_UDEC 4, [matrix + esi + edi]
    NEWLINE
    add edx, [matrix + esi + edi]

    dec ecx
    cmp ecx, 0
    jl .next
    push ebx
    push ecx
    push edx
    call find_path
    add esp, 12

    .next:
        dec ebx
        cmp ebx, 0
        jl .comp
        push ebx
        push ecx
        push edx
        call find_path
        add esp, 12

    .comp:
        cmp eax, edx
        jl .end
        mov eax, edx
    
    .end:
        pop ebx
        pop ebp
        ret
