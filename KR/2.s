%include "io.inc"

section .text
global CMAIN
CMAIN:
    xor ebx, ebx
    xor ecx, ecx
    GET_UDEC 4, eax

    .loop:
        lea ecx, [ecx + eax]
        add ebx, ecx

        GET_UDEC 4, eax
        test eax, eax
        jnz .loop

    PRINT_UDEC 4, ebx

    xor eax, eax
    ret
