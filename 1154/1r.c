#include <stdio.h>

unsigned int 
F(unsigned int a);

int
main(void)
{
    unsigned int a;

    scanf("%u", &a);

    a = F(a);

    printf("%u\n", a);

    return 0;
}

unsigned int
F(unsigned int a)
{
    if (a) {
        return 3 * F(a - 1);
    }
    else {
        return 1;
    }
}
