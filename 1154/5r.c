#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 0x1000

int
map(char *map_string, char *first_alph, char *second_alph);

void
convert(char *map_string, char *str);

int
main(void)
{
    char *first_alph;
    char *second_alph;
    char *map_string;
    char *str;
    int n;

    first_alph = calloc(256, sizeof(char));
    second_alph = calloc(256, sizeof(char));
    map_string = calloc(256, sizeof(char));

    fgets(first_alph, 255, stdin);
    fgets(second_alph, 255, stdin);

    if ( !map(map_string, first_alph, second_alph) ) {
        free(first_alph);
        free(second_alph);
        free(map_string);
        
        return 0;
    }

    str = calloc(BUF_SIZE, sizeof(char));
    scanf("%d", &n);
    fgets(str, BUF_SIZE, stdin);

    for (; n > 0; n--) {
        fgets(str, BUF_SIZE, stdin);

        convert(map_string, str);

        printf("%s", str);
    }

    free(first_alph);
    free(second_alph);
    free(map_string);
    free(str);

    return 0;
}

int
map(char *map_string, char *first_alph, char *second_alph)
{
    unsigned char index;
    size_t first_len;

    first_len = strlen(first_alph);

    if ( first_len != strlen(second_alph) ) {
        return 0;
    }

    for (int i = 0; i < 256; i++) {
        map_string[i] = i;
    }

    for (int i = 0; i < first_len; i++) {
        index = first_alph[i];
        map_string[index] = second_alph[i];
    }

    return 1;
}

void
convert(char *map_string, char *str)
{
    unsigned char index;
    size_t str_len;

    str_len = strlen(str);

    for (int i = 0; i < str_len; i++) {
        index = str[i];

        str[i] = map_string[index];
    }
}
