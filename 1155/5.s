%include "io.inc"

global CMAIN
EXTERN printf
EXTERN scanf
EXTERN calloc
EXTERN realloc
EXTERN free

section .rodata
    format db '%d', 0

section .bss
    seq resd 1

section .text
CMAIN:
    push ebx
    push esi
    push edi
    push ebp
    mov ebp, esp
    and esp, ~15
    sub esp, 0x10

    mov esi, 1 ; len_seq
    push 4
    push esi
    call calloc
    add esp, 8
    mov [seq], eax ; seq

    sub esp, 4
    
    xor edi, edi ; last_index
    lea eax, [esp + 4]
    push eax
    push format
    call scanf
    add esp, 8
    jmp .cmp

.loop:
    lea eax, [edi * 4]
    mov ecx, [seq]
    mov ebx, [esp + 4]
    mov [ecx + eax], ebx
    inc edi
    cmp esi, edi
    jne .input

    mov eax, [seq]
    shl esi, 1
    lea eax, [esi * 4]
    push eax
    push dword [seq]
    call realloc
    add esp, 8
    mov [seq], eax

.input:
    lea eax, [esp + 4]
    push eax
    push format
    call scanf
    add esp, 8

.cmp:
    mov eax, [esp + 4]
    test eax, eax
    jne .loop

    add esp, 4

    mov eax, edi
    sub eax, 1
    lea eax, [eax * 4]
    mov ecx, [seq]
    mov ebx, [ecx + eax] ; last element

    xor esi, esi ; index
    xor edx, edx ; counter
    jmp .count_cmp

.count:
    lea eax, [esi * 4]
    mov eax, [ecx + eax]
    cmp eax, ebx
    jge .add_index
    inc edx

.add_index:
    inc esi

.count_cmp:
    mov eax, edi
    sub eax, 1
    cmp esi, eax
    jl .count

    push edx
    push format
    call printf
    add esp, 8

    push dword [seq]
    call free
    add esp, 4

    xor eax, eax
    leave
    pop edi
    pop esi
    pop ebx
    ret
