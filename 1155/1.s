%include 'io.inc'

global CMAIN
EXTERN printf
EXTERN scanf

section .rodata
    input db '%u', 0
    output db '0x%08X', 10, 0

section .text
CMAIN:
    push ebp
    mov ebp, esp
    and esp, ~15
    sub esp, 10h
    sub esp, 4
    
    xor eax, eax
    jmp .input

.loop:
    push dword [esp + 4]
    push output
    xor eax, eax
    call printf
    add esp, 8

.input:
    lea eax, [esp + 4]
    push eax
    push input
    xor eax, eax
    call scanf
    add esp, 8
    cmp eax, 1
    je .loop

.end:
    add esp, 4
    xor eax, eax
    mov esp, ebp
    pop ebp
    ret
