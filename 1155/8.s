%include "io.inc"

global CMAIN
EXTERN calloc
EXTERN free
EXTERN fopen
EXTERN fclose
EXTERN fprintf
EXTERN fscanf
EXTERN qsort

section .rodata
    input_filename db 'input.txt', 0
    input_mode db 'r', 0
    output_filename db 'output.txt', 0
    output_mode db 'w', 0
    input_format db '%d', 0
    output_format db '%d ', 0

section .bss
    seq resd 1
    stream resd 1

section .text
cmp_int:
    push ebp
    mov ebp, esp
    
    mov eax, dword [ebp + 8]
    mov ecx, [eax]
    mov eax, dword [ebp + 12]
    mov edx, [eax]
    
    cmp ecx, edx
    jge .ge
    mov eax, -1
    jmp .end

.ge:
    cmp ecx, edx
    jle .le
    mov eax, 1
    jmp .end

.le:
    xor eax, eax

.end:
    leave
    ret

CMAIN:
    push ebx
    push ebp
    mov ebp, esp
    and esp, ~15
    sub esp, 16

    push 4
    push 1000
    call calloc
    mov [seq], eax
    add esp, 8

    push input_mode
    push input_filename
    call fopen
    mov [stream], eax
    add esp, 8

    sub esp, 4
    xor ecx, ecx
    jmp .input
    
.loop:
    lea eax, [ecx * 4]
    mov edx, [seq]
    mov ebx, [esp + 4]
    mov [edx + eax], ebx
    inc ecx

.input:
    lea eax, [esp + 4]
    push ecx
    push eax
    push input_format
    push dword [stream]
    call fscanf
    add esp, 12
    pop ecx
    cmp eax, -1
    jne .loop
    add esp, 4

    mov ebx, ecx
    push cmp_int
    push 4
    push ecx
    push dword [seq]
    call qsort
    add esp, 16

    push dword [stream]
    call fclose
    add esp, 4

    push output_mode
    push output_filename
    call fopen
    mov [stream], eax
    add esp, 8

    mov ecx, ebx
    xor ebx, ebx
    jmp .cmp

.out_loop:
    lea eax, [ebx * 4]
    mov edx, [seq]
    add edx, eax
    push ecx
    push dword [edx]
    push output_format
    push dword [stream]
    call fprintf
    add esp, 12
    pop ecx
    inc ebx

.cmp:
    cmp ebx, ecx
    jl .out_loop

    xor eax, eax
    leave
    pop ebx
    ret
