%include "io.inc"

global CMAIN

section .text

sum:
    push ebp
    mov ebp, esp
    push ebx
    push esi
    push edi
    
    mov edi, dword [ebp + 8]  ; den2
    mov esi, dword [ebp + 12] ; num2
    mov ecx, dword [ebp + 16] ; den1
    mov ebx, dword [ebp + 20] ; num1

    mov eax, dword [ebx]
    mul edi
    mov dword [ebx], eax

    mov eax, dword [ecx]
    mul esi
    
    add dword [ebx], eax

    mov eax, dword [ecx]
    mul edi
    mov dword [ecx], eax

    pop edi
    pop esi
    pop ebx
    mov esp, ebp
    pop ebp
    ret

gcd:
    push ebp
    mov ebp, esp
    push ebx

    mov ebx, [ebp + 8]  ; b 
    mov eax, [ebp + 12] ; a

    test eax, eax
    jne .check_b
    mov eax, ebx
    jmp .end
    
.check_b:
    test ebx, ebx
    je .end

.loop:
    cmp eax, ebx
    je .end
    jl .else
    sub eax, ebx
    jmp .loop
.else:
    sub ebx, eax
    jmp .loop

.end:
    pop ebx
    mov esp, ebp
    pop ebp
    ret

normalize:
    push ebp
    mov ebp, esp
    push ebx
    push edx
    push edi

    mov ebx, [ebp + 8]
    mov ecx, [ebp + 12]

    push dword [ecx]
    push dword [ebx]
    call gcd
    add esp, 8
    mov edi, eax

    mov eax, [ecx]
    div edi
    mov [ecx], eax

    mov eax, [ebx]
    div edi
    mov [ebx], eax

    pop edi
    pop edx
    pop ebx
    mov esp, ebp
    pop ebp
    ret

CMAIN:
    push ebp
    mov ebp, esp
    push ebx
    push esi

    sub esp, 8

    GET_UDEC 4, ecx ; num of fracs
    GET_UDEC 4, [esp + 8]
    GET_UDEC 4, [esp + 4]

    dec ecx
.loop:
    test ecx, ecx
    je .end_loop

    GET_UDEC 4, edx
    GET_UDEC 4, esi

    push ecx
    lea eax, [esp + 12]
    push eax
    lea eax, [esp + 12]
    push eax
    push edx
    push esi
    call sum
    add esp, 8
    call normalize
    add esp, 8
    pop ecx

    dec ecx
    jmp .loop

.end_loop:
    PRINT_UDEC 4, [esp + 8]
    PRINT_CHAR ' '
    PRINT_UDEC 4, [esp + 4]

    xor eax, eax
    pop esi
    pop ebx
    mov esp, ebp
    pop ebp
    ret
