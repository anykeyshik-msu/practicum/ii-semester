%include "io.inc"

global CMAIN

section .text

check:
    push ebp
    mov ebp, esp
    push ebx
    push esi
    
    mov ebx, dword [ebp + 8]
    mov eax, 1
    cmp ebx, 1
    je .end

.calc:
    mov ecx, 1
    xor esi, esi

.loop:
    xor edx, edx
    cmp ebx, ecx
    je .cmp_sum

    mov eax, ebx
    div ecx
    test edx, edx
    jne .end_loop
    
    add esi, ecx

.end_loop:
    inc ecx
    jmp .loop

.cmp_sum:
    xor eax, eax
    cmp ebx, esi
    jle .end
    mov eax, esi

.end:
    pop esi
    pop ebx
    mov esp, ebp
    pop ebp
    ret

CMAIN:
    push ebp
    mov ebp, esp

    push ebx

    GET_UDEC 4, ebx
    xor ecx, ecx

.find:
    test ebx, ebx
    je .end

    inc ecx
    push ecx
    call check
    pop ecx
    
    test eax, eax
    je .find

    dec ebx
    jmp .find

.end:
    PRINT_UDEC 4, ecx
    
    pop ebx

    xor eax, eax
    mov esp, ebp
    pop ebp
    ret
