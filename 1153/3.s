%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_DEC 4, eax
    call input

    xor eax, eax
    ret

input:
    push ebp
    mov ebp, esp

    test eax, eax
    jz .out

    push eax
    GET_DEC 4, eax
    call input

    pop eax
    PRINT_DEC 4, eax
    PRINT_CHAR ' '
    
.out:
    pop ebp
    ret
