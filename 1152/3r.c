#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int N, K;
    int i, j;
    int *a, *b, *temp;

    a = calloc( 21, sizeof(int) );
    b = calloc( 21, sizeof(int) );

    scanf("%d %d", &N, &K);

    a[0] = 1;

    for (i = 0; i < N; i++) {
        b[0] = 1;

        for (j = 0; j < i; j++) {
            b[j + 1] = a[j + 1] + a[j];
        }

        b[j + 1] = 1;

        temp = a;
        a = b;
        b = temp;
    }

    printf("%d\n", a[K]);
    
    free(a);
    free(b);

    return 0;
}
