#include <stdio.h>
#include <inttypes.h>

int
main(void)
{
    int var;

    scanf("%u", &var);
    
    var &= (~var + 1);
    var |= (uint32_t)(var == 0 ? 1 : 0) << (sizeof(var) * 8 - 1);

    printf("%u\n", var);

    return 0;
}
