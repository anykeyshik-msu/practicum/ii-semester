%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_DEC 4, eax

    xor ebx, ebx
    xor ecx, ecx
    xor edx, edx

.get_number:
    GET_DEC 4, edi

        cmp ebx, edi
        jae .sec_cmp
        mov edx, ecx
        mov ecx, ebx
        mov ebx, edi
        jmp .cnt_cmp

    .sec_cmp:
        cmp ecx, edi
        jae .thrd_cmp
        mov edx, ecx
        mov ecx, edi
        jmp .cnt_cmp

    .thrd_cmp:
        cmp edx, edi
        jae .cnt_cmp
        mov edx, edi

    .cnt_cmp:
        dec eax
        jnz .get_number

    PRINT_DEC 4, ebx
    PRINT_CHAR ' '
    PRINT_DEC 4, ecx
    PRINT_CHAR ' '
    PRINT_DEC 4, edx
    NEWLINE

    xor eax, eax
    ret
