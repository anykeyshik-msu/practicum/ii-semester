%include "io.inc"

section .bss
    num resd 11

section .text
global CMAIN
CMAIN:
    GET_UDEC 4, eax
    
    mov ebx, 10
    mov edi, 8

.convert:
    xor edx, edx
    div edi
    
    mov [num + ebx * 4], edx
    
    dec ebx
    test eax, eax
    jnz .convert

    inc ebx
.print:
    PRINT_DEC 4, [num + ebx * 4]

    inc ebx
    cmp ebx, 11
    jne .print

    xor eax, eax
    ret
