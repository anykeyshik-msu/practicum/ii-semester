%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_UDEC 4, eax
    GET_UDEC 4, ebx

.start:
    xor edx, edx

    test eax, eax
    jz .end

    test ebx, ebx
    jz .end

    cmp eax, ebx
    jb .bellow
    div ebx
    mov eax, edx
    jmp .start

.bellow:
    xchg eax, ebx
    div ebx
    mov eax, edx
    xchg eax, ebx
    jmp .start

.end:
    or eax, ebx
    PRINT_UDEC 4, eax

    xor eax, eax
    ret
